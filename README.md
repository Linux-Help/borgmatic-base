# Borgmatic Backup Templates and Scripts

## Installation

```
git clone https://git.linux-help.org/Linux-Help/borgmatic-base.git /etc/borgmatic
pushd /etc/borgmatic
cp templates/config.yaml.tpl config.yaml
cp templates/patterns.tpl patterns
cp templates/excludes.tpl excludes
popd
```

Once installed and initially setup, you should edit the config.yaml and set:
source_directories      - Include any additional directories needed if needed.
repositories            - Add repository locations appropriately.
encryption_passphrase   - Passphrase for backup, if not using 'none' for encryption.
ssh_command             - Provide the ssh key for your backups.

When Adding agents to the before_backup, before_check, and after_backup, make sure the ones being added to after_backup are in reverse order to the ones in before_backup so they can perform their cleanups, if need-be, in the right order.

